import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ConsoleApp {

    private Product product;

    public static void main(String[] args) {
        RandomStorePopulator populator = new RandomStorePopulator();
        Map<String, List<Product>> products = populator.populate();

        Store store = new Store();
        store.addProducts(products);
        store.viewStore();
        System.out.println("Write rate, price, name of product");

        Scanner scanner = new Scanner(System.in);
        String selectedName = scanner.nextLine();

        store.addInBasket(new Product(selectedName));
        store.viewBasket();
    }
}

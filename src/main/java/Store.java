import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Store {
//    products - все продукты, которые хранятся в магазине
    private Map<String, List<Product>> products = new HashMap<>();
    private List<Product> basket = new ArrayList<>();
    private List<Product> selectedProduct = new ArrayList<>();

    public void addInBasket(Product product) {
        basket.add(product);


//        пишу строчку, которая делает -- в rate при добавлении товара в корзину
    }

    public void deleteInBasket(Product product) {
        basket.remove(product);
    }

    public void viewBasket() {
        System.out.println(basket);
    }

    public void addProducts(Map<String, List<Product>> products) {
        this.products = products;
    }

    public void viewStore() {
        System.out.println(products);
    }
}
